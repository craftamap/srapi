# srapi

Simple RCON Web REST API build with flask.

## About

I have some CSGO-Servers running on a root-server managed by me. To provide a secure
yet easy solution for other server admins to use the rcon "on-the-run", this api
allow to login with your Steam-Account and, if you have the rights, manage certain
servers.

